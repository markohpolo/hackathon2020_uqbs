# **hackathon2020_uqbs**
This project uses match history ID's which feeds into the RiotAPI to get the champion and win/lose data of individual matches. The retreived data feeds into the `modelling.py` file to produce a prediction model of the win/lose of a Leauge of Legend game based on the champion combination.

## **RiotAPIGrabber.py**
Due to the time limit restriction of the RiotAPI per key, the number of requests that can be processed to the API was not enough to produce a meaningful model. In order to optimize the data retrival, 5 threads were utilized (one for each server).
### **requestMatchID(matchIDs, index, files)** 
Requests the match history detail to the api, using the matchID from the match history json files, and saves the win/lose and champion to a text file in  the format of `[[matchList],winState]`.
### **main()**
Reads all match history IDs from the json file and feeds to the `requestMatchID(matchIDs, index, files)`, using 
```
with ThreadPoolExecutor(max_workers=5) as worker:
    worker.map(requestMatchID, match_IDS, (0,1,2,3,4), (na, euw, eun, jp, kr))
    worker.shutdown()

```
## **Modelling.py**
The win/lose history text file saved from `RiotAPIGrabber.py` gets read by `Modelling.py`, which converts it to tensors using `tf.stack` in the form of `[matchList]` and `[winSateList]`.
### **Model detail**
**input_shape(=302)** length of x_train_set is 302 (= 151 champions on each side x 2)
\
**layer reduction**
```
model = tf.keras.Sequential([
    layers.Dense(302, input_shape=(302,)),
    layers.Dense(151, input_shape=(302,)),
    layers.Dense(50, input_shape=(302,)),
    layers.Dense(10, input_shape=(302,)),
    layers.Dense(2)])
```
\
**logistic regression**
```
    model.summary()
    # x = tf.Tensor(x_train, 1, float)
    model.compile(optimizer='adam',loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),metrics=['accuracy'])

    history = model.fit(x_train, y_train, epochs=5, validation_data=(x_test,y_test))
    test_loss, test_acc = model.evaluate(x_test, y_test, verbose=2)

```

## **To do**
* Change layers and train more epochs to improve accuracy and loss
* Study ML before producing a model
