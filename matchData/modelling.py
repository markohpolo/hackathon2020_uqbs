import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import re
import shutil
import string

from tensorflow.keras import layers
from tensorflow.keras import losses
from tensorflow.keras import preprocessing
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization

max_features = 302
def convertToList(matches):
        champIds = matches.readline()
        champIds = champIds.replace(']', '')
        matches = champIds.split('[[')
        matches.pop(0)
        matchList = []
        winState = []
        for m in matches:
            m.replace('[', '')
            m = m.split(',')
            teamList = []
            for i in range(0, 302):
                if 'True' in m[i]:
                    teamList.append(True)
                elif 'False' in m[i]:
                    teamList.append(False)
                else:
                    print("NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO\n")
            matchList.append(tf.constant(teamList))
            if 'True' in m[302] :
                winState.append(tf.constant(True))
            else:
                winState.append(tf.constant(False))
        tf.stack(matchList)
        tf.stack(winState)
        matchList = np.asarray(matchList)
        winState = np.asarray(winState)
        return matchList,  winState

def main():
    with open('DATA.txt', 'r') as matches:
        x_train, y_train     = convertToList(matches)

    with open('jp.txt', 'r') as matches:
        x_test, y_test     = convertToList(matches)
    # embedding_dim = 16or
    # print('lllllllllllllllllllllllllllllllllllllllllllll',tf.shape(x_train))

    model = tf.keras.Sequential([
        layers.Dense(302, input_shape=(302,)),
        layers.Dense(151, input_shape=(302,)),
        layers.Dense(50, input_shape=(302,)),
        layers.Dense(10, input_shape=(302,)),
        layers.Dense(2)])
    print(f"\n X = {x_train[0:2]} \n\n")
    print(f"\n Label = {y_train[0:2]} \n\n")

    model.summary()
    # x = tf.Tensor(x_train, 1, float)
    model.compile(optimizer='adam',loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),metrics=['accuracy'])

    history = model.fit(x_train, y_train, epochs=5, validation_data=(x_test,y_test))
    test_loss, test_acc = model.evaluate(x_test, y_test, verbose=2)
    print(f"Test accuracy: {test_acc}")
    print(f"Test loss: {test_loss}")


if __name__ == "__main__":
    main()
