import requests
import json
import time
import csv
from concurrent.futures import ThreadPoolExecutor

SERVER = ['na1', 'euw1', 'eun1', 'jp1', 'kr']
api_key = "RGAPI-213edfcf-8790-4abf-beb5-f2eb4296ed14"
championID = {'266': 0, '103': 1, '84': 2, '12': 3, '32': 4, '34': 5, '1': 6, '523': 7, '22': 8, '136': 9, '268': 10, '432': 11, '53': 12, '63': 13, '201': 14, '51': 15, '164': 16, '69': 17, '31': 18, '42': 19, '122': 20, '131': 21, '119': 22, '36': 23, '245': 24, '60': 25, '28': 26, '81': 27, '9': 28, '114': 29, '105': 30, '3': 31, '41': 32, '86': 33, '150': 34, '79': 35, '104': 36, '120': 37, '74': 38, '420': 39, '39': 40, '427': 41, '40': 42, '59': 43, '24': 44, '126': 45, '202': 46, '222': 47, '145': 48, '429': 49, '43': 50, '30': 51, '38': 52, '55': 53, '10': 54, '141': 55, '85': 56, '121': 57, '203': 58, '240': 59, '96': 60, '7': 61, '64': 62, '89': 63, '876': 64, '127': 65, '236': 66, '117': 67, '99': 68, '54': 69, '90': 70, '57': 71, '11': 72, '21': 73, '62': 74, '82': 75, '25': 76, '267': 77, '75': 78, '111': 79, '518': 80, '76': 81, '56': 82, '20': 83, '2': 84, '61': 85, '516': 86, '80': 87, '78': 88, '555': 89, '246': 90, '133': 91, '497': 92, '33': 93, '421': 94, '58': 95, '107': 96, '92': 97, '68': 98, '13': 99, '360': 100, '113': 101, '235': 102, '875': 103, '35': 104, '98': 105, '102': 106, '27': 107, '14': 108, '15': 109, '72': 110, '37': 111, '16': 112, '50': 113, '517': 114, '134': 115, '223': 116, '163': 117, '91': 118, '44': 119, '17': 120, '412': 121, '18': 122, '48': 123, '23': 124, '4': 125, '29': 126, '77': 127, '6': 128, '110': 129, '67': 130, '45': 131, '161': 132, '254': 133, '112': 134, '8': 135, '106': 136, '19': 137, '498': 138, '101': 139, '5': 140, '157': 141, '777': 142, '83': 143, '350': 144, '154': 145, '238': 146, '115': 147, '26': 148, '142': 149, '143': 150}

def getMatchID(region):
    with open(f'matchlist_{region}.json', 'r') as json_file:
        DATA = json.load(json_file)
        return DATA
 
def requestMatchID(matchIDs, index, files):
    for matchID in matchIDs:
        URL = f"https://{SERVER[index]}.api.riotgames.com/lol/match/v4/matches/{matchID}?api_key={api_key}"   
        response = requests.get(URL)
        response = response.json()
        team1, team2 = [False]*151, [False]*151
        try:
            if response['teams'][0]['win'] == 'Win':
                win = True 
            else:
                win = False

            for i in range(10):
                if (response['participants'][i]['teamId'] == 100):
                    champ = championID[str(response['participants'][i]['championId'])]
                    team1[champ] = True
                else:
                    champ = championID[str(response['participants'][i]['championId'])]
                    team1[champ] = True

            output= [team1,team2,win]
            files.write(str(output))
            files.flush()
        except Exception  as e:
            print(e.__class__.__name__, e, URL)
            

def main():
    match_IDS = (getMatchID('na1'), getMatchID('euw1'),getMatchID('eun1'),getMatchID('jp1'),getMatchID('kr'))
    with open("na.txt", 'w') as na, open("euw.txt", 'w') as euw,  open("eun.txt", 'w') as eun,  open("jp.txt", 'w') as jp,  open("kr.txt", 'w') as kr:
        with ThreadPoolExecutor(max_workers=5) as worker:
            worker.map(requestMatchID, match_IDS, (0,1,2,3,4), (na, euw, eun, jp, kr))
            worker.shutdown()
               
#This starts my program!
if __name__ == "__main__":
    main()